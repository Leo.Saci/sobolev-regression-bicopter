import argparse
from pathlib import Path
import yaml
import numpy as np
import torch as T

from datagen import DDPMaker,Datagen
from neural_network import FeedForwardNet
from fit import sobolevRegression

savePATH = 'trained_agents/'
log_dir  = 'logs'

try:
    Path(savePATH).mkdir(parents=True, exist_ok=True)
    Path(log_dir).mkdir(parents=True, exist_ok=True)
except FileExistsError:
    pass

args            = open("./params.yml")
args            = yaml.load(args, Loader=yaml.FullLoader)

# Generate Dataset
ddp             =   DDPMaker.make(horizon = args["horizon"],
                                runningCostW = [args["runningCostW"]], 
                                terminalCostW = [args["terminalCostW"]])


datagen                 =   Datagen(ddp=ddp,grid=True)
xtrain, ytrain1, ytrain2  =   datagen.generate(npoints=args["n_training_points"],train=True,verbose=True)


# Instantiate a feedforward network

if args["activation"] == 'tanh':
    act = T.nn.Tanh()
elif args["activation"] == 'relu':
    act = T.nn.ReLU()

learner             =   FeedForwardNet(n_inputs=args["ninputs"], 
                                        n_outputs=1,
                                        n_hidden_layers=args["n_hidden_layers"],
                                        n_hidden=args["n_hidden"],
                                        activation=act)
print(learner)
# Fit
value_network       =   sobolevRegression(net=learner,
                                            xtrain=xtrain,
                                            ytrain1=ytrain1,
                                            ytrain2=ytrain2,
                                            batchsize=args["batch_size"],
                                            lr=args["learning_rate"],
                                            epochs=args["n_epochs"],
                                            log_dir=log_dir)


# Save the neural network

T.save(value_network,savePATH+"Sbicoptere5.pth")
