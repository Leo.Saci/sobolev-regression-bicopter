import numpy as np
import torch as T
import matplotlib.pyplot as plt



def plot_values(save=True,plot2d=True,plot3d=True):
    data         = np.load("data/bicopter_plot.npy",allow_pickle=True).item()

    x0s          =  np.array(data["x0s"])
    v_true       =  np.array(data["trueV"]).reshape(-1,1)
    v_pred       =  np.array(data["predv"]).reshape(-1,1)
    X            =  np.array(data["X"])
    Y            =  np.array(data["Y"])
    V            =  np.array(data["V_C"])
    Vnet         =  np.array(data["V_NET"])

    if plot2d:
        fig, (ax1, ax2) = plt.subplots(1, 2)
        fig.subplots_adjust(wspace=0.3)
        im1 = ax1.scatter(x0s[:,0],x0s[:,1],c=v_true,marker=".",cmap='jet')
        im2 = ax2.scatter(x0s[:,0],x0s[:,1],c=v_pred,marker=".",cmap='jet')
        fig.colorbar(im1,ax=[ax1,ax2],label="Value Function",
                    orientation="horizontal", fraction=.1,pad=0.1)

        ax1.axis('off')
        ax2.axis('off')
        ax1.set_title("Crocoddyl")
        ax2.set_title("FeedforwardNetwork")
        plt.subplots_adjust(wspace=0.07)

        if save:
            plt.savefig("figs/v2d.png")
        plt.show()

    if plot3d:
        # set up a figure twice as wide as it is tall
        fig = plt.figure(figsize=plt.figaspect(0.4))
        ax1 = fig.add_subplot(1, 2, 1, projection='3d')
        im1 = ax1.plot_surface(X,Y,V,cmap='jet',alpha=None, antialiased=True)

        ax2 = fig.add_subplot(1, 2, 2, projection='3d')
        im2 = ax2.plot_surface(X,Y,Vnet,cmap='jet',alpha=None, antialiased=True)

        ax1.azim, ax1.elev = -165,46
        ax2.azim, ax2.elev = 13,44
        ax1.set_title("Crocoddyl")
        ax2.set_title("FeedforwardNetwork")
        plt.colorbar(im2,label="Value Function")
        if save:
            plt.savefig("figs/v3d.png")
        plt.show()

def plot_value_loss(save=True,plot2d=True,plot3d=True):
    data         = np.load("data/bicopter_plot.npy",allow_pickle=True).item()

    x0s          =  np.array(data["x0s"])
    v_true       =  np.array(data["trueV"]).reshape(-1,1)
    v_predicted  =  np.array(data["predv"]).reshape(-1,1)
    error        =  (v_predicted - v_true)**2
    if plot2d:
        plt.scatter(x0s[:,0],x0s[:,1],c=error,marker=".",cmap='jet')
        plt.colorbar()
        plt.title(r'$\epsilon^2$, with $\epsilon = Net(x) - V(x)$')
        plt.xlabel("<-- x -->")
        plt.ylabel("<--y -->")
        if save:
            plt.savefig("figs/errorv2d.png")
        plt.show()

    if plot3d:
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.grid(False)
        im = ax.scatter(x0s[:,0],x0s[:,1],error, c=error,cmap='jet')
        ax.set_xlabel("<-- x -->",fontsize=15)
        ax.set_ylabel("<--y -->",fontsize=15)
        ax.zaxis.set_rotate_label(False)
        ax.set_zlabel(r'$\epsilon^2$',fontsize=15,rotation=-360)
        ax.azim, ax.elev = -127,10
        plt.colorbar(im)
        plt.title(r'$\epsilon^2$, with $\epsilon = Net(x) - V(x)$')
        if save:       
            plt.savefig("figs/errorv3d.png")
        plt.show()



def plot_grads(save=True,plot2d=True,plot3d=True,grad_idx=0):
    data         = np.load("data/bicopter_plot.npy",allow_pickle=True).item()

    X            =  np.array(data["X"])
    Y            =  np.array(data["Y"])
    x0s          =  np.array(data["x0s"])
    v_true       =  np.array(data["trueVx"])
    v_pred       =  np.array(data["predvx"])

    if grad_idx == 0:
        name="Gradient at x"
    elif grad_idx == 1 :
        name="Gradient at Y"
    elif grad_idx == 2:
        name=r'Gradient at $\theta$'
    if plot2d:
        fig, (ax1, ax2) = plt.subplots(1, 2,figsize=(10,8))
        fig.subplots_adjust(wspace=0.3)
        im1 = ax1.scatter(x0s[:,0],x0s[:,1],c=v_true[:,grad_idx],marker=".",cmap='jet')
        im2 = ax2.scatter(x0s[:,0],x0s[:,1],c=v_pred[:,grad_idx],marker=".",cmap='jet')
        fig.colorbar(im1,ax=[ax1,ax2],label=name,
                    orientation="horizontal", fraction=.1,pad=0.1)

        ax1.axis('off')
        if save:
            plt.savefig(f"figs/{name}g2d.png")
        plt.show()

    if plot3d:
        # set up a figure twice as wide as it is tall
        fig = plt.figure(figsize=plt.figaspect(0.4))
        ax1 = fig.add_subplot(1, 2, 1, projection='3d')
        im1 = ax1.scatter(x0s[:,0],x0s[:,1],v_true[:,grad_idx],c=v_true[:,grad_idx],
                        cmap='jet',alpha=None, antialiased=True)

        ax2 = fig.add_subplot(1, 2, 2, projection='3d')
        im2 = ax2.scatter(x0s[:,0],x0s[:,1],v_pred[:,grad_idx],c=v_pred[:,grad_idx],
                        cmap='jet',alpha=None, antialiased=True)

        ax1.azim, ax1.elev = -24,-151
        ax2.azim, ax2.elev = -24,-151
        ax1.set_title(r"$V'$")
        ax2.set_title(r"$N'$")
        plt.colorbar(im2,label=name)
        if save:
            plt.savefig(f"figs/{name}g3d.png")
        plt.show()


def plot_eigvals_hessians(save=True,plot2d=True,plot3d=True,idx=0):
    """This needs to be cleared up a lot. Different colorbars should be used.""" 
    data         = np.load("data/bicopter_plot.npy",allow_pickle=True).item()

    X            =  np.array(data["X"])
    Y            =  np.array(data["Y"])
    x0s          =  np.array(data["x0s"])
    vxx          =  np.array(data["trueVxx"])
    nxx          =  np.array(data["predvxx"])

    vxx          = np.vstack(np.array([np.linalg.eigvals(x) for x in vxx]))
    nxx          = np.vstack(np.array([np.linalg.eigvals(x) for x in nxx]))

    if idx == 0:
        name = r'$\lambda$ 1'
    elif idx == 1:
        name = r'$\lambda$ 2'
    else:
        name = r'$\lambda$ 3'

    if plot2d:
        fig, (ax1, ax2) = plt.subplots(1, 2,figsize=(10,8))
        fig.subplots_adjust(wspace=0.3)
        im1 = ax1.scatter(x0s[:,0],x0s[:,1],c=vxx[:,idx],marker=".",cmap='jet')
        im2 = ax2.scatter(x0s[:,0],x0s[:,1],c=nxx[:,idx],marker=".",cmap='jet')
        fig.colorbar(im1,ax=[ax1,ax2],label=name,
                    orientation="horizontal", fraction=.1,pad=0.1)

        ax1.axis('off')
        ax2.axis('off')
        ax1.set_title(r"$V''$")
        ax2.set_title(r"$N''$")
        #plt.subplots_adjust(wspace=0.07)

        if save:
            plt.savefig(f"figs/{name}h2d.png")
        plt.show()

    if plot3d:
        # set up a figure twice as wide as it is tall
        fig = plt.figure(figsize=plt.figaspect(0.4))
        ax1 = fig.add_subplot(1, 2, 1, projection='3d')
        im1 = ax1.scatter(x0s[:,0],x0s[:,1],vxx[:,idx],c=vxx[:,idx],
                        cmap='jet',alpha=None, antialiased=True)

        ax2 = fig.add_subplot(1, 2, 2, projection='3d')
        im2 = ax2.scatter(x0s[:,0],x0s[:,1],nxx[:,idx],c=nxx[:,idx],
                        cmap='jet',alpha=None, antialiased=True)

        ax1.azim, ax1.elev = -24,-151
        ax2.azim, ax2.elev = -24,-151
        ax1.set_title(r"$V''$")
        ax2.set_title(r"$N''$")
        plt.colorbar(im2,label=name)
        if save:
            plt.savefig(f"figs/{name}h3d.png")
        plt.show()


plot_values()
plot_value_loss()

for idx in range(3):
    plot_grads(grad_idx=idx)
    plot_eigvals_hessians(idx=idx)
