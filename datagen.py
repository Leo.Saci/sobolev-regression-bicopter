import crocoddyl as C
import numpy as np
import torch as T
from tqdm import tqdm
from pathlib import Path
from bicopter import ActionModelBicopter


def random(npoints):
    """
    Randomly sample for dataset of size = [npoints, 3]
    """
    return np.random.rand(npoints,3)*[2,2,2*np.pi]-[1,1,np.pi]
    
def grid(npoints,shuffle=False):
    """
    Generate n_points from a 6d grid
    """
    n_points = int(np.cbrt(npoints)) + 1
    x           =   np.linspace(-1.1, 1.1, num=n_points, endpoint=True)
    x_dot       =   np.linspace(-0.21, 0.21,num=n_points, endpoint=True)
    y           =   np.linspace(-1.1, 1.1, num=n_points, endpoint=True)
    y_dot       =   np.linspace(-0.21, 0.21,num=n_points, endpoint=True)
    theta       =   np.linspace(-np.pi/8, np.pi/8, num=n_points, endpoint=True)
    theta_dot   =   np.linspace(-2.1/4, 2.1/4, num=n_points, endpoint=True)
    
    positions   =   [[a, b, c, d, e, f] for a in x for b in y for c in theta for d in x_dot for e in y_dot for f in theta_dot]
    if shuffle: np.random.shuffle(positions)
    position = np.array(positions)[0:npoints]
    return position



class DDPMaker:
    RUNNING_COSTW  = [[0.05, 0.05, 0.0, 0.01, 0.01, 0.01, 0.01, 0.01]]
    TERMINAL_COSTW = [[10, 10, 10, 10, 10, 10, 0., 0.]]
    HORIZON        = 300

    

    @staticmethod
    def make(runningCostW = None, terminalCostW = None, horizon = None):

        runningCostW    =   DDPMaker.RUNNING_COSTW if runningCostW is None else runningCostW
        terminalCostW   =   DDPMaker.TERMINAL_COSTW if terminalCostW is None else terminalCostW
        horizon         =   DDPMaker.HORIZON if horizon is None else horizon

        model                       =   ActionModelBicopter()
        model.costWeights           =   np.array(*runningCostW)
        terminal_model              =   ActionModelBicopter()
        terminal_model.costWeights  =   np.array(*terminalCostW)
        state                       =   np.zeros(6)
        problem                     =   C.ShootingProblem(state, [model] * horizon, terminal_model)
        ddp                         =   C.SolverFDDP(problem)

        return ddp

    

class Datagen:

    savepath = "data/"

    def __init__(self,ddp=None,grid=True):
        self.ddp        =   DDPMaker.make() if ddp is None else ddp
        self.sampling   =   'grid' if grid else 'random'     # Sampling technique

    def generate(self,npoints=100,train=True,verbose=True):

        if self.sampling == 'grid':
            n_samples = grid(npoints=npoints,shuffle=True)
        else:
            n_samples = random(npoints=npoints)
        

        vxx     =   []
        x0s     =   []
        vx      =   []
        v       =   []
        
        verbosator = tqdm if verbose else lambda x:x
        m = 0.5
        g  = 9.81
        T = self.ddp.problem.T
        u_guess = [np.sqrt([m * g * 0.5, m * g * 0.5])] * T

        for x0 in verbosator(n_samples):
            self.ddp.problem.x0 = x0
            x_guess = [np.array([x0[0], x0[1], 0, 0, 0, 0])] * (T + 1)
            done = self.ddp.solve(x_guess, u_guess, 1000)
            # if not done:
            #     print(x0)
            v.append( self.ddp.cost )
            vx.append( self.ddp.Vx[0] )
            vxx.append( self.ddp.Vxx[0] )
            x0s.append( x0 )


        self.x0s = np.array(x0s)
        self.v   = np.array(v).reshape([-1,1])
        self.vx  = np.array(vx)
        self.vxx = np.array(vxx)
        if not train:
            try:
                Path(Datagen.savepath).mkdir(parents=True, exist_ok=True)
            except FileExistsError:
                pass
            datafile = Datagen.savepath+'test.npy'
            np.save(datafile,
                    {"x0s":self.x0s,
                    "v": self.v,
                    "vx":self.vx,
                    "vxx": self.vxx})
        # Sobolev Training
        return self.x0s,self.v,self.vx






if __name__=='__main__':
    # CREATE EVAL DATASET   
    ddp = DDPMaker.make()

