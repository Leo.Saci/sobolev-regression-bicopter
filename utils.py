import numpy as np
import torch as T
import yaml
from tqdm import tqdm
import torch.autograd.functional as F
from datagen import DDPMaker
import matplotlib.pyplot as plt


def plotTraj(traj, values, variables, intervals):

    fig, ax = plt.subplots(1, 1)
    fig.subplots_adjust(wspace=0.3)
    im = ax.scatter(traj[:,0],traj[:,1],c=values,marker=".",cmap='jet')
    fig.colorbar(im, ax=[ax],label="Cost",
                orientation="vertical", fraction=.1,pad=0.1)

    # ax.axis('off')
    ax.set_title("Crocoddyl")
    ax.set_xlim([-1, 1])
    # ax.set_ylim([-1.5, 1])
    ax.set_xlabel(variables[0])
    ax.set_ylabel(variables[1])
    plt.show()


def plotting_data(dim=12, variables = ['theta', 'theta_dot']):
    """
    Plotting data with theta = 0
    """

    args            = open("./params.yml")
    args            = yaml.load(args, Loader=yaml.FullLoader)

    m = 0.5
    g  = 9.81

    # Generate Dataset
    ddp             =   DDPMaker.make(horizon = args["horizon"],
                                    runningCostW = [args["runningCostW"]], 
                                    terminalCostW = [args["terminalCostW"]])

    agent = T.load("trained_agents/Sbicoptere5.pth")



    intervals = {'x': (0, np.linspace(-1, 1, dim)), 'y': (1, np.linspace(-1, 1, dim)), 'theta': (2, np.linspace(-np.pi/8, np.pi/8, dim)), 'xdot': (3, np.linspace(-0.2, 0.2, dim)), 'ydot': (4, np.linspace(-0.2, 0.2, dim)), 'theta_dot': (5, np.linspace(-0.2/4, 0.2/4, dim))}
    idx = [intervals[variables[0]][0], intervals[variables[1]][0]]
    x       = intervals[variables[0]][1]
    y       = intervals[variables[1]][1]
    X,Y     = np.meshgrid(x,y)
    
    
    # For croc
    vxx     =   []
    x0s     =   []
    vx      =   []
    v       =   []

    # For net
    v_      =   []
    vx_     =   []
    vxx_    =   []

    TRAJ, TRAJ_V = [], []
    hor = ddp.problem.T
    for x, y in tqdm(zip(np.ravel(X), np.ravel(Y))):
        x0 = np.zeros(6)
        x0[intervals[variables[0]][0]], x0[intervals[variables[1]][0]] = x, y
        ddp.problem.x0 = x0
        x_init = [x0 for t in range(hor+1)]
        u_init = [np.sqrt([m * g * 0.5, m * g * 0.5])] * hor
        # x_init, u_init = [], []
        done = ddp.solve(x_init, u_init)
        print(done)
        if not done:
            print(x0[idx])

        # Torch network
        torchx0 = T.tensor(x0, requires_grad=True)
        vnet,dvnet    = agent(torchx0)
        d2vnet  = F.hessian(agent.value,torchx0).squeeze()
        vnet    = vnet.detach().numpy()
        dvnet   = dvnet.detach().numpy()
        d2vnet  = d2vnet.detach().numpy()

        value = ddp.cost
        x0 = np.array([x, y])
        v.append( value )
        vx.append( ddp.Vx[0] )
        vxx.append( ddp.Vxx[0] )
        x0s.append( x0 )
        v_.append( vnet )
        vx_.append( dvnet )
        vxx_.append( d2vnet )

        # if (x in [-1, 1] or y in [-np.pi/8, np.pi/8]) and done:
        TRAJ.append(np.array(ddp.xs.tolist())[:, idx])
        TRAJ_V.append([value] * (hor + 1))
    x0s = np.array(x0s)
    V   = np.array(v).reshape(X.shape)
    vx  = np.array(vx)
    vxx = np.array(vxx)
    truev = np.array(v)

    V_   = np.array(v_).reshape(X.shape)
    vx_  = np.array(vx_)
    vxx_ = np.array(vxx_)
    predv = np.array(v_)

    datafile = "./data/bicopter_plot.npy"
    np.save(datafile,{
        "x0s":x0s,
        "trueV":truev,
        "trueVx":vx,
        "trueVxx":vxx,
        "X":X,
        "Y":Y,
        "V_C":V,
        "V_NET":V_,
        "predvx":vx_,
        "predv":predv,
        "predvxx":vxx_
    })

    TRAJ = np.concatenate(TRAJ)
    TRAJ_J = np.concatenate(TRAJ_V)
    plotTraj(TRAJ, TRAJ_V, variables, intervals)
    return TRAJ

if __name__=='__main__':
    TRAJ = plotting_data()