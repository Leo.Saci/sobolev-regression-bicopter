import torch as T
import torch.nn.functional as F
from torch.autograd import Variable
from tensorboardX import SummaryWriter
import numpy as np
from neural_network import FeedForwardNet
DTYPE = T.float64
T.set_default_dtype(DTYPE)


def sobolevRegression(net,xtrain,ytrain1,ytrain2,batchsize=1000,lr=1e-2,epochs=10000,log_dir="logs"):

    
    x0s                     =   T.tensor(xtrain,dtype=DTYPE)
    v                       =   T.tensor(ytrain1,dtype=DTYPE)
    vx                      =   T.tensor(ytrain2,dtype=DTYPE)
    
    
    dataset                 =   T.utils.data.TensorDataset(x0s,v,vx)
    dataloader              =   T.utils.data.DataLoader(dataset,batch_size=batchsize,shuffle=True)
    optimizer               =   T.optim.Adam(params = net.parameters(), lr = lr)
    trainSize               =   x0s.shape[0]
    # Anchor
    origin                  =   T.zeros(size=(1,6),requires_grad=True)

    
    if log_dir is None:
        log_dir = "logs"
    writer = SummaryWriter(logdir=log_dir)
    print(f"\nTraining on {trainSize} samples ...")
    
    for epoch in range(epochs):
        net.train()
        epoch_loss1 = 0
        epoch_loss2 = 0
        epoch_loss3 = 0
        for idx, (data) in enumerate(dataloader):
            x0s,v,vx = data
            x0s      = Variable(x0s,requires_grad=True)
            v        = Variable(v, requires_grad=True)
            vx       = Variable(vx, requires_grad=True)

            # Prediction
            v_hat,dv_hat    = net(x0s)
            loss1           = F.mse_loss(input = v_hat, target = v)
            loss2           = F.mse_loss(input = dv_hat, target = vx)
            vp,_            = net(origin)
            loss3           = F.mse_loss(input=vp,target=T.zeros(size=vp.size()))
            
            
            optimizer.zero_grad()
            loss1.backward(retain_graph=True)
            loss2.backward(retain_graph=True)
            loss3.backward()

            optimizer.step()
            epoch_loss1   += x0s.shape[0] * loss1.item()
            epoch_loss2   += x0s.shape[0] * loss2.item()
            epoch_loss3   += x0s.shape[0] * loss3.item()


        epoch_loss1 = epoch_loss1 / trainSize
        epoch_loss2 = epoch_loss2 / trainSize

        writer.add_scalar("Classical Loss", epoch_loss1, epoch)
        writer.add_scalar("Sobolev Loss", epoch_loss2, epoch)

        if epoch % 50 == 0:
            print(f"\nEpoch : {epoch}")
            print(f"Loss between v,vpred        : {epoch_loss1}")
            print(f"Loss in Grads   :  {epoch_loss2}")
        if epoch_loss1 < 1e-8:
            print("Early Stopping ! \n")
            break

    return net

