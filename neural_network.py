import torch as T
import torch.autograd.functional as F
import torch.nn as nn
from collections import OrderedDict
T.set_default_dtype(T.float64)




class FeedForwardNet(nn.Module):
    def __init__(self, n_inputs=6, n_outputs=1, n_hidden_layers=3,n_hidden=64,activation=nn.Tanh()):
        super(FeedForwardNet, self).__init__()


        architecture       = self.default_architecure(nInputs=n_inputs,
                                                      nOutputs=n_outputs,
                                                      nHidden=n_hidden,
                                                      nHiddenLayers=n_hidden_layers,
                                                      activation=activation
                                                      )
        self.value_network = nn.Sequential(architecture)

    @staticmethod
    def default_architecure(nInputs,nOutputs,nHidden,nHiddenLayers,activation):
        
        layers = [
            (f'Input Layer ', nn.Linear(in_features= nInputs ,out_features=nHidden)),
            (f'Input Layer activation', activation),
                ]
        
        # Hidden layers
        for nl in range(nHiddenLayers):
            layers.extend( [
                (f'Hidden Layer #{nl+1}', nn.Linear(in_features= nHidden ,out_features=nHidden)),
                (f'Hidden Layer #{nl+1} activation', activation),
            ])
        # Terminal layer
        layers.append( (f'terminal layer ', nn.Linear(in_features= nHidden ,out_features=nOutputs)) )

        return OrderedDict(layers)

    def forward(self, x):
        v = self.value_network(x)
        dv = T.autograd.grad(outputs = v, inputs = x, 
                            retain_graph=True,grad_outputs=T.ones_like(v), 
                            create_graph=True)[0]

        return v,dv

    def value(self,x):
        """For Hessian calculation"""
        v = self.value_network(x)
        return v

