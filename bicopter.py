import crocoddyl as C
import pinocchio
import numpy as np
import torch as T
import torch.autograd.functional as F

class ActionModelBicopter(C.ActionModelAbstract):
    def __init__(self, costWeights=[0.1, 0.1, 0.001, 0.001, 0.001, 0.001, 0.01, 0.01], dt=1e-2, terminal_network=None, terminal_approximation=None):
        C.ActionModelAbstract.__init__(self, C.StateVector(6), 2)
        
        self.m = 0.5
        self.l = 0.15
        self.I = 0.1
        self.g  = 9.81
        self.tau                = dt
        self.costWeights        = np.array(costWeights)
        self.terminal_network   = terminal_network
        self.approximation      = terminal_approximation
        # self.f, self.f_x, self.f_xx = lambda x: x, lambda x: 1, lambda x: 0
        self.f, self.f_x, self.f_xx = lambda x: x**2, lambda x: 2*x, lambda x: 2


    @property
    def nx(self):
        """The state space is [x, y, theta, x_dot, y_dot, theta_dot]
        So the dimension of state space is 6. 
        Needed to instantiate an agent with input dimensions==dimensions of state space
        
        """
        return 6
    
    @property
    def nu(self):
        """Dimensions of action space"""
        return 2
    
    @property
    def timeStep(self):
        return self.tau
    
    def state_cost_function(self, state):
        """
        Compute cost as a function of state
        """
        x, y, theta, x_dot, y_dot, theta_dot = state
        s, c = np.sin(theta), np.cos(theta)
        theta = np.arctan2(s, c)
        res = np.array([x, y, theta, x_dot, y_dot, theta_dot]) * self.costWeights[:6]
        cost_x = 0.5 * np.sum(res ** 2)                               
        return cost_x
    
    def value_function(self, state, action):
        """Compute value function : V = cost_due_to_state + cost_due_to_action"""
        g, m = self.g, self.m
        cost_x      =   self.state_cost_function(state)
        cost_u      =   0.5 * np.sum((self.costWeights[6:] * (self.f(action) - 0.5 * m * g)) ** 2)
        value       =   cost_x + cost_u
        return value
        
    def calc(self, data, state, action):
        if self.terminal_network is None:

            data.cost = self.value_function(state, action)

            x, y, th, vx, vy, w = state[0].item(), state[1].item(), state[2].item(), state[3].item(), state[4].item(), state[5].item()
            u0, u1 = self.f(action[0].item()), self.f(action[1].item())

            m, g, l, I = self.m, self.g, self.l, self.I
            c, s = np.cos(th), np.sin(th)
            th = np.arctan2(s, c)

            vxdot = - (u0 + u1) * s / m
            vydot = (u0 + u1) * c / m - g
            wdot = l * (u0 - u1) / I

            vx += self.tau * vxdot
            vy += self.tau * vydot
            w += self.tau * wdot
            x += self.tau * vx
            y += self.tau * vy
            th += self.tau * w

            c, s = np.cos(th), np.sin(th)
            th = np.arctan2(s, c)

            data.xnext = np.array([x, y, th, vx, vy, w])

            return data.xnext, data.cost
        
        else:
            state_ = T.tensor(state, requires_grad=True)
            cost = self.terminal_network.value(state_)
            cost = cost.detach().numpy().item()
            data.cost = cost

            return data.cost

            
    def calcDiff(self, data, state, action):

        if self.terminal_network is None:
            xnext, cost = self.calc(data, state, action)
            m, g, l, I = self.m, self.g, self.l, self.I
            x, y, th, xdot, ydot, w  = state
            s, c, dt = np.sin(th), np.cos(th), self.tau
            th                       = np.arctan2(s, c).item()
            state_      =   np.array([x, y, th, xdot, ydot, w])
            
            lx          =   np.array(self.costWeights[:6]) ** 2 * state_
            diagonals   =   np.array(self.costWeights[:6]) ** 2
            lxx         =   np.diag(diagonals)
            lx          =   np.array(lx).squeeze()
            data.Lx     =   lx * dt
            data.Lxx    =   lxx * dt

            lu          =   np.array(self.costWeights[6:]) ** 2 * (self.f(action) - 0.5 * m * g ) * self.f_x(action) 
            diagonals   =   np.array(self.costWeights[6:]) ** 2 * (self.f_x(action)**2 + (self.f(action) - 0.5 * m * g) * self.f_xx(action))
            luu         =   np.diag(diagonals)
            lu          =   np.array(lu).squeeze()
            data.Lu     =   lu * dt
            data.Luu    =   luu * dt


            u0, u1 = self.f(action[0].item()), self.f(action[1].item())

            Fx = np.eye(6)
            Fu = np.zeros((6, 2))

            Fx[0, 2] = - (u0 + u1) * c * dt**2 / m
            Fx[0, 3] = dt
            Fx[1, 2] = - (u0 + u1) * s * dt**2 / m
            Fx[1, 4] = dt
            Fx[2, 5] = dt
            Fx[3, 2] = - (u0 + u1) * c * dt / m
            Fx[4, 2] = - (u0 + u1) * s * dt / m
            
            Fu[0] = - s * dt**2 * self.f_x(action) / m
            Fu[1] = c * dt**2 * self.f_x(action) / m
            Fu[2] = l * dt**2 * np.array([self.f_x(action[0]), -self.f_x(action[1])]) / I
            Fu[3] = - s * dt * self.f_x(action) / m
            Fu[4] = c * dt * self.f_x(action) / m
            Fu[5] = l * dt * np.array([self.f_x(action[0]), -self.f_x(action[1])]) / I

            data.Fx = Fx
            data.Fu = Fu

            return xnext, cost

        else:
            try:
                assert self.approximation is not None
            except AssertionError:
                # set to analytic derivatives
                self.approximation = False
            
            state_ = T.tensor(state, requires_grad=True)
            v, vx = self.terminal_network(state_)
            vx = vx.detach()
            vxx  = F.hessian(self.terminal_network.value, state_).detach()

            # Lx, Lxx   =   self.terminal_network.derivatives(state=state,approximation=self.approximation)
            data.Lx   =   np.array(vx.squeeze())
            data.Lxx  =   np.array(vxx.squeeze())