import numpy as np
import torch as T
import yaml
from tqdm import tqdm
import torch.autograd.functional as F
import crocoddyl as C
from datagen import Datagen, grid
from bicopter import ActionModelBicopter
import matplotlib.pyplot as plt
from tqdm import tqdm


args            = open("./params.yml")
args            = yaml.load(args, Loader=yaml.FullLoader)
V_net = T.load("trained_agents/Sbicoptere4.pth")
horizon = 2 * args["horizon"]

model                       =   ActionModelBicopter()
model.costWeights           =   np.array(args["runningCostW"])
terminal_model              =   ActionModelBicopter(terminal_network=V_net)
# terminal_model.costWeights  =   np.array(args["terminalCostW"])
state                       =   np.zeros(6)
problem                     =   C.ShootingProblem(state, [model] * horizon, terminal_model)
ddp                         =   C.SolverFDDP(problem)


dim = 2
m, g, nx = model.m, model.g, model.nx
TRAJ, V = [], []

intervals = {'x': (0, np.linspace(-1, 1, dim)), 'y': (1, np.linspace(-1, 1, dim)), 'th': (2, np.linspace(-np.pi/8, np.pi/8, dim)), 'xdot': (3, np.linspace(-0.2, 0.2, dim)), 'ydot': (4, np.linspace(-0.2, 0.2, dim)), 'w': (5, np.linspace(-0.2/4, 0.2/4, dim))}
variables = ['x', 'y']


x       = intervals[variables[0]][1]
y       = intervals[variables[1]][1]
X,Y     = np.meshgrid(x,y)

for x, y in tqdm(zip(np.ravel(X), np.ravel(Y))):
    x0 = np.zeros(nx)
    x0[intervals[variables[0]][0]], x0[intervals[variables[1]][0]] = x, y
    ddp.problem.x0 = x0
    x_guess = [x0] * (horizon + 1)
    u_guess = [np.sqrt([m*g*.5, m*g*.5])] * horizon
    ddp.solve(x_guess, u_guess)
    
    value = ddp.cost
    TRAJ.append(np.array(ddp.xs.tolist())[:, [intervals[variables[0]][0], intervals[variables[1]][0]]])
    V.append([value] * (horizon + 1))

TRAJ = np.concatenate(TRAJ)
V = np.concatenate(V)

if __name__=='__main__':
    from utils import plotTraj
    plotTraj(TRAJ, V)
